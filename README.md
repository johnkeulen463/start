# Introduction

Working with our zaaksysteem requires working in a lot of different libraries /
packages / services.  This repository acts as a kind of "mono-repo", which
makes working on different services a lot easyer by providing a lot of helper
utils.

The main advantage is our use of myrepos: using the .mrconfig file we can add
new repositories to this repository, from where we can start our development on
the different package.

## Getting started

If you're running Docker Windows or MacOS, make sure Docker can use at least
10GB of memory. By default the virtual machines on those operating systems only
have 2GB of memory.

### TLDR

```Shell
git clone git@gitlab.com:zaaksysteem/start.git
cd start

bin/zs myrepos install
mr --jobs=5 update               # --jobs=5 means: run 5 parallel jobs
bin/zs zaaksysteem setup
docker-compose up -d

bin/extract_ca_certificate.sh
```

The Zaaksysteem development environment is now available at
[dev.zaaksysteem.nl](https://dev.zaaksysteem.nl/)

To get rid of the security error (unknown CA), you can tell your browser or
operating system to trust the certificate stored in `dev.zaaksysteem.nl.pem`.

By default, pre-built Docker images are used. See below for how to set up
development mode for frontend and backend.

### Long Version

First, the repository needs some basic tools to be installed and configured.
It will check and install them by running:

```Shell
bin/zs myrepos install
```

It will also ask for your gitlab username, which will be stored so it can be used
later for branch names, etc.

After succesfull initialization, you can retrieve the source code by running
the following command. By using the `--jobs=` option, we speed it up by cloning
multiple repositories in parallel.

```Shell
mr --jobs=5 update
```

Now we have our repositories, let's make sure we can spin up our zaaksysteem.

This command will create the required configuration files from their `.dist`
counterparts in `etc/`, and create the storage bucket in the `Minio` container.

```Shell
bin/zs zaaksysteem setup
```

That's it. Now start your development Zaaksysteem by running `docker-compose up`,
like this:

```Shell
# -d = detached, start in background
docker-compose up -d
```

This could take a while. Find yourself some coffee, and when docker tells you everything is ok,
wait two minutes more, and browse to `dev.zaaksysteem.nl` for your first encounter.

You can log in with the following user accounts:

| Username  | Password  | Description |
|---|---|---|
| `admin`  | `admin`  | Main "administrator" user; this one we use the most during development. |
| `beheerder`  | `beheerder`  | Secondary administrator account |
| `gebruiker`  | `gebruiker`  | Normal user account with few rights. |

Of course you're free to create more users, roles and groups in your development
environment as needed.

You'll probably see a HTTPS security error (unknown CA). To extract the CA
certificate used by your dev environment, run this:

```Shell
bin/extract_ca_certificate.sh
```

This puts the CA certificate in `dev.zaaksysteem.nl.pem` -- tell your browser or OS to
trust it and the error should disappear.

### Development mode

By default, docker images are used, which means changes in the source code won't
be picked up by the services. To make that work, select a frontend and/or backend
development mode:

```Shell
bin/zs devmode frontend {mode}
bin/zs devmode backend {mode}
```

Frontend supports 3 modes: `image` (use a pre-build container image, default),
`container` (runs the development server in a container) and `local` (to run the
development server locally).

Run docker maintenance after switching modes:

```Shell
./bin/zs docker maintenance
```

To run the development server for `local` frontend development, use the following:

```Shell
# And then run the local development server:
cd zsnl-frontend-mono
WEBPACK_BUILD_TARGET=development yarn lerna:start
```

Backend supports 2 modes: `image` and `container`.

So if you're a backend developer, you can use:

```Shell
bin/zs devmode frontend image
bin/zs devmode backend container
```

A frontend developer might want:

```Shell
bin/zs devmode frontend local
bin/zs devmode backend image
```

Or if you're running Linux on your development machine:

```Shell
bin/zs devmode frontend local
bin/zs devmode backend container
```

For Visual Studio Code users, `.code-workspace` files exist.

## Typical Development

Below are examples to start development on our systems, depending on the part of the
system you're working on, here are some options:

### Python

For python development, you are probably working on domain code, combined with
a http daemon or rabbitmq consumer. Typical development involves the daemons,
the domain (business logic) and the database (infra). For the http daemon of
the domain "case management", you would work in:

- zsnl-http-cm
- zsnl-domains

#### Using Docker

When using docker, it is useful to have a requirements file containing links to
the local copies of dependencies.

For instance, if you are starting work on the `zsnl-http-cm` service, you
should run the following commands:

```Shell
cd zsnl-http-cm       # Only run the next commands for this specific repo
mr zs python generate # Generate requirements.txt files
mr zs python dinstall # Install the local dependencies in "editable" mode in
                      # the docker container for this repo
```

These two commands will generate requirements files containing links to the
other repositories, so you do not have to wait for the `zsnl-domains` changes
to be pushed before you can work on the http service.

A quick way to generate the requirements for every python package, simply run
the same command in the main "start" directory. You can also do this with the
`vinstall` and `dinstall` commands.

Run `mr zs python` to get more information and help.

#### Using a virtual environment

If you want to use Visual Studio Code and all of the helper utilities for
testing, without running everything inside containers, you can also set up
virtual environments.

There is one system library the software depends on, because there does not
seem to be a good Python version. This is `libmagic` (used to detect the
type of uploaded files, so we can set the correct MIME type). Install it using
`brew`, `apt`, or the package manager that comes with your distribution.

We will use the `zsnl-http-cm` service as an example again:

```Shell
cd zsnl-http-cm
mr zs python setup # Create a new virtual environment in <start>/.venv/
source ../.venv/zsnl-http-cm/bin/activate
```

This will create a virtual environment in `.venv/zsnl-http-cm` in the `start`
repository root and activates it in your current shell, but this does not yet
install the requirements. To do that, run the following command for that:

```Shell
mr zs python vinstall
```

This will run `pip install -r REQUIREMENTSFILES` for this repository.

You can speed all of the above up by using `mr` to do it for every repository. That looks like
this:

```Shell
mr zs python generate
mr zs python setup
mr zs python vinstall
```

This will run it for every repository containing a `setup.py` file.

### Perl

Perl is the most straightforward way for developing. Just edit some files in `zsnl-perl-api`
and run the necessary docker-compose commands to reload the engine.

```Shell
vim zsnl-perl-api/lib/Zaaksysteem/Constants.pm

docker-compose stop perl-api
docker-compose up -d perl-api
```

### JavaScript (React)

Running the React codebase in `local` development mode requires the following to
be installed locally:

- [Brew](https://brew.sh/) (optional) to make managing/installing packages easier
- [Node](https://github.com/nvm-sh/nvm) a recent version of Node
- [Yarn](https://classic.yarnpkg.com/en/docs/install/) a recent version of Yarn

Once installed, you can run the development environment:

```Shell
cd zsnl-frontend-mono
yarn install
WEBPACK_BUILD_TARGET=development yarn lerna:start
```

This will serve the React apps locally, and will refresh any changes you make to the code via Hot Module Reloading.

To use `container` development mode, all you need to do is enable it and rebuild/restart
your containers. This will run the above `yarn lerna:start` command inside a container.

See `./zsnl-frontend-mono/docs/` for more details on installation and running
of the frontend development tools.


### Javascript (Angular)

Use this script to start the app / area you wish to develop in:

```
./bin/run-frontend.sh {command}
```

where {command} is one of the following: reset, gulp, styles, frontend, intern, mor, vergadering, wordaddin or init.

This will start the development server in the `frontend-frontendclient` container, and start watching files. Your changes should then be made available on your local development environment.


## Development Certificate

Follow these steps to install the Development Certificate:

### MacOS / Chrome

In Chrome, go to Settings -> Privacy and Security, Manage Certificates. This will open up the Keychain.

Drag the textfile into this window.

Right-click on the 'Zaaksysteem Development CA' and select 'Get Info'.

Open 'Trust' and set 'When using this certificate' to 'Always Trust'.

## How does it fit together

Todo, but let's start with this diagram:

![Overview of repositories](assets/repositories_zoom_70percent.png)

## Admin Interfaces

Some of the services used by Zaaksysteem come with web-based admin tools. These are available at
the following URLs:

- [RabbitMQ](https://rabbitmq.dev.zaaksysteem.nl/) (message queue; use guest / guest to log in)
- [Minio](https://minio.dev.zaaksysteem.nl/) (S3-compatible file store; see etc/minty_config.conf for login keys)
- [Redis](https://redis.dev.zaaksysteem.nl/) (temporary storage for session data)
- [Mailhog](https://mailhog.dev.zaaksysteem.nl/) (development web-mail server)

## Settings

Editorsettings are, when possible, described in the editorconfig. Here are some
advised settings for some common editors,

### Visual Studio Code

To use VSCode efficiently, we've added workspace files to this repository and,
extension recommendations and pre-configured settings in all (sub-)repositories.

These settings will enable automatic formatting using `black` and sorting of
imports using `isort` on save, and will run `flake8` while typing.

#### Recommended Settings

(the instructions in this section will be obsolete once we've put settings.json is in the
repositories)

It's recommended to set VSCode up so code is automatically formatted using `black`
and `isort`, and linted with `flake8` to be run.

Go to your settings by clicking `Ctrl + Shift + P` or `Cmd + Shift + P` (MacOS)
and type `workspace settings` (or `user settings` if you want to work permanently like this)

Or you could just create a folder called .vscode in the top "start" folder, and create
`.vscode/settings.json` containing the following settings:

Be sure to check the `isort` path!

```JSON
{
    "python.linting.flake8Enabled": true,
    "python.formatting.provider": "black",
    "python.formatting.blackArgs": [
        "-l",
        "79",
        "--exclude",
        "\\.eggs"
    ],
    "editor.formatOnSave": true,
    "python.sortImports.path": "/usr/local/bin/isort",
    "python.sortImports.args": [
        "-sg .eggs",
        "-m 3",
        "-tc",
        "-rc",
        "-ds"
    ],
    "[python]": {
        "editor.codeActionsOnSave": {
            "source.organizeImports": true
        }
    },
    "editor.rulers": [
        79
    ]
}
```

This will do the following:

- Configure black
- Configure isort
- Configure flake8
- Set the python interpreter to the zsnl_domains python engine (bit of hack, ok for now)
- Set Black as the default formatting provider
- Makes sure that everytime you save your file, both `black` as `isort` clean up your code
- Will hint possible `flake8` errors during typing

## Contributing

Please read [CONTRIBUTING](https://gitlab.com/minty-python/zsnl_domains/blob/master/CONTRIBUTING.md)
for details on our code of conduct, and the process for submitting pull requests to us.

## License

Copyright (c) 2020, Minty Team and all persons listed in
[CONTRIBUTORS](https://gitlab.com/minty-python/zsnl_domains-cqs/blob/master/CONTRIBUTORS>)

This project is licensed under the EUPL, v1.2. See the
[EUPL-1.2.txt](https://gitlab.com/minty-python/zsnl_domains/blob/master/LICENSE>)
file for details.
