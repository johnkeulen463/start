"""ClamAV replacement for development only."""

import io
import logging
import os
import socket

logger = logging.getLogger(__name__)

CLAMAV_IP = os.environ.get('CLAMAV_IP', '0.0.0.0')
CLAMAV_PORT = int(os.environ.get('CLAMAV_PORT', '3310'))
CLAMAV_PORT_STREAM = int(os.environ.get('CLAMAV_PORT_STREAM', '3311'))

EICAR = b"X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*"


class SocketIO(io.RawIOBase):
    def __init__(self, sock):
        self.sock = sock

    def read(self, sz=-1):
        if (sz == -1):
            sz = 0x7FFFFFFF
        return self.sock.recv(sz)

    def write(self, b):
        return self.sock.send(b)

    def seekable(self):
        return False


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as main_socket:
    main_socket.bind((CLAMAV_IP, CLAMAV_PORT))
    main_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    logger.info(f"Listening on {CLAMAV_IP}:{CLAMAV_PORT}")

    main_socket.listen()

    while True:
        connection_raw, address = main_socket.accept()

        logger.info(f"Accepted connection from {address}")
        connection = SocketIO(connection_raw)

        with connection:
            virus = False

            command = connection.readline()

            if command == b"PING\n":
                connection.write(b"PONG\n")
                connection_raw.shutdown(socket.SHUT_RDWR)
                continue
            elif command != b"STREAM\n":
                logger.error(f"Wrong command: {command}. Closing connection.")
                connection_raw.shutdown(socket.SHUT_RDWR)
                continue

            data_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            data_socket.bind((CLAMAV_IP, CLAMAV_PORT_STREAM))
            data_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            data_socket.listen()

            connection.write(f"PORT {CLAMAV_PORT_STREAM}\n".encode('UTF-8'))

            (data_conn, data_addr) = data_socket.accept()
            logger.info(f"Accepted data connection from {data_addr}")

            while data := data_conn.recv(1024):
                if data == EICAR:
                    virus = True

            data_conn.shutdown(socket.SHUT_RDWR)
            data_conn.close()

            if virus:
                connection.write(b"stream: 1 FOUND\n")
            else:
                connection.write(b"stream: OK\n")

            connection_raw.shutdown(socket.SHUT_RDWR)
            connection.close()
