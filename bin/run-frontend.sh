#! /bin/bash
choice=$1

usage() {
    local rc=$1

    [ -z "$rc" ] && rc=0

    echo "Please supply a command: reset, gulp, styles, frontend, intern, mor, vergadering, wordaddin or init" >&2;
    exit $rc

}

start_frontend() {
    docker-compose restart frontend-frontendclient
}

intern() {
    docker-compose exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/client && CLIENT_APP=intern npm start"
}

mor() {
    docker-compose exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/client && CLIENT_APP=mor npm start"
}

vergadering() {
    docker-compose exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/client && CLIENT_APP=vergadering npm start"
}

wordaddin() {
    docker-compose exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/client && CLIENT_APP=wordaddin npm start"
}

frontend() {
    docker-compose exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/frontend && npm run start-wp"
}

gulp() {
    docker-compose exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/frontend && npm run start-gulp"
}

styles() {
    docker-compose exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/frontend && npm run start-gulp-styles"
}

case $choice in
    init)
        start_frontend
        docker-compose exec -T frontend-frontendclient \
            /opt/zaaksysteem/dev-bin/npm_container.sh
        ;;
    intern)
        intern
        ;;
    mor)
        mor
        ;;
    vergadering)
        vergadering
        ;;
    wordaddin)
        wordaddin
        ;;
    frontend)
        frontend
        ;;
    gulp)
        gulp
        ;;
    styles)
        styles
        ;;
    dev)
        start_frontend
        intern & mor & vergadering & wordaddin & frontend & gulp &
        ;;
    reset)
        start_frontend
        docker-compose exec -T frontend-frontendclient \
            /opt/zaaksysteem/dev-bin/reset-frontend.sh
        ;;
    *) usage 1;;

esac



