# Authentication Server

The development environment optionally includes an authentication/authorization service, Keycloak.

It's not automatically set up, because:

- Keycloak uses a lot of memory, and the development environment is already huge
- It's not strictly necessary to run a development environment.

To enable it set `auth` devmode (`./bin/zs devmode auth on`) and (re)start the containers (`docker-compose up -d`).

## Connecting Keycloak and Development-Zaaksysteem

Once the service is up and running you can get to the admin console on [keycloak.dev.zaaksysteem.nl](https://keycloak.dev.zaaksysteem.nl/) and log in using username `admin` and password `keycloak123`.

You'll be in the configuration for the "master" realm. As this realm is not for general use, you need to create a separate realm for the Zaaksysteem application.

### Import realm

To start creating a new realm, move your mouse over the "Master" realm name and click on the blue "Add realm" button.

![Keycloak "Add Realm" button](../assets/keycloak/addrealm-button.png)

On the "Create Realm" screen, choose "Select file" and import `docs/keycloak-realm.json` to import the "ZS-Development" realm, preconfigured to work with the development instance of Zaaksysteem.

![Keycloak "Add Realm" screen](../assets/keycloak/addrealm.png)

You can now switch to the newly imported realm, go to the "Clients" page and open the client named `https://dev.zaaksysteem.nl/auth/saml`.

### Downloading the SP key

The next step is downloading the SAML SP key and certificate. To do this, go to the "SAML Keys" page:

![Client keys](../assets/keycloak/client-keys.png)

And click the "Export" button. Then set it to export the key and certificate as a PKCS#12 file:

![Client keys export screen](../assets/keycloak/client-keys-export.png)

To transform the `.p12` file into something Zaaksysteem can use in its configuration, you need to convert:

```
openssl pkcs12 -nodes -out zs-keycloak.pem -in keystore.p12 
```

Enter the password you specified during export (`12345678` in the example) and you're ready to go.

### Setting up Zaaksysteem

Create a new "SAML Service Provider" integration. For the "public key" and "certificate", upload `zs-keycloak.pem` created in the previous step.

Set "Contactpersoon" to `beheerder` and "Base URL" to `https://dev.zaaksysteem.nl/auth/saml`.

![Zaaksysteem SAML SP configuration screen, configured as described](../assets/keycloak/zs-saml-sp.png)

Next, create a new "SAML Identity Provider" integration.

Set "Binding" to `HTTP Redirect`, "SAML Implementatie" to `Custom SAML`, the "SAML IdP metadata-URL" to `https://keycloak.dev.zaaksysteem.nl/auth/realms/ZS-Development/protocol/saml/descriptor` and upload the dev environment root certificate in the "CA Certificaat van de IdP" field (see [../README.md](../README.md) for more about that).

Select "Gebruik op medewerkerloginpagina", 

![Zaaksysteem SAML IdP configuration screen, configured as described](../assets/keycloak/zs-saml-idp.png)

**Don't forget to set both integrations to "Active"!**

### Creating Users

To create a user, go to the "Users" page for the "ZS-Development" realm in Keycloak.

There you can find a button  to add a new user, which leads to this screen:

![Keycloak "Create user" screen](../assets/keycloak/create-user.png)

After creating the user, you need to set a password. You do this by going to the "Credentials" tab, and setting one:

![Keycloak "User Credentials" screen](../assets/keycloak/set-password.png)
